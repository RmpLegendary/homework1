package jmp.workshop.cloud.service.impl;

import jmp.workshop.dto.BankCard;
import jmp.workshop.dto.Subscription;
import jmp.workshop.dto.User;
import jmp.workshop.service.api.Service;

import java.time.LocalDate;
import java.util.*;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class ServiceImpl implements Service {

    private final Map<User, List<BankCard>> userStorage = new HashMap<>();
    private final List<Subscription> subscriptions = new ArrayList<>();

    @Override
    public void subscribe(BankCard bankCard) {
        var user = bankCard.getUser();
        if (!userStorage.containsKey(user)) {
            userStorage.put(user, new ArrayList<>());
        }
        userStorage.get(user).add(bankCard);
        subscriptions.add(new Subscription(bankCard.getNumber(), LocalDate.now()));
    }

    @Override
    public double getAverageUsersAge() {
        return Service.super.getAverageUsersAge();
    }

    @Override
    public Optional<Subscription> getSubscriptionByBankCardNumber(String number) {
        return subscriptions.stream().filter(subscription -> subscription.getBankcard().equals(number)).findFirst();
    }

    @Override
    public List<User> getAllUsers() {
        return new ArrayList<>(userStorage.keySet());
    }

    @Override
    public List<Subscription> getAllSubscriptionsByCondition(Predicate<Subscription> predicate) {
        return subscriptions.stream()
                .filter(predicate)
                .collect(Collectors.toList());
    }
}
