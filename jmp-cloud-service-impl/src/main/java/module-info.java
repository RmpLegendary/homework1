import jmp.workshop.cloud.service.impl.ServiceImpl;
import jmp.workshop.service.api.Service;

module jmp.cloud.service.impl.main {
    exports jmp.workshop.cloud.service.impl;
    requires transitive jmp.service.api.main;
    requires jmp.dto.main;
    provides Service with ServiceImpl;
}