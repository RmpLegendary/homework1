package jmp.workshop.dto;

import lombok.*;

import java.time.LocalDate;

@Data
@AllArgsConstructor
public class Subscription {
    private String bankcard;
    private LocalDate startDate;

}
