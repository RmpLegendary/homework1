import jmp.workshop.bank.api.Bank;
import jmp.workshop.jmp.cloud.bank.impl.BankImpl;

module jmp.cloud.bank.impl.main {
    requires jmp.bank.api.main;
    requires jmp.dto.main;

    exports jmp.workshop.jmp.cloud.bank.impl;
    provides Bank with BankImpl;
}