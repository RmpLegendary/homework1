package jmp.workshop.jmp.cloud.bank.impl;

import jmp.workshop.bank.api.Bank;
import jmp.workshop.dto.*;

import java.util.Map;
import java.util.UUID;
import java.util.function.BiFunction;

public class BankImpl implements Bank {
    private final Map<BankCardType, BiFunction<String, User, BankCard>> cardFactories = Map.of(
            BankCardType.CREDIT, CreditBankCard::new,
            BankCardType.DEBIT, DebitBankCard::new
    );

    @Override
    public BankCard createBankCard(User user, BankCardType bankCardType) {
        return cardFactories.getOrDefault(bankCardType, (n,u) -> {throw new IllegalArgumentException("wrong bank card type");})
                .apply(UUID.randomUUID().toString(), user);
    }
}
