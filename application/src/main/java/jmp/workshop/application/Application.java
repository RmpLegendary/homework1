package jmp.workshop.application;

import jmp.workshop.bank.api.Bank;
import jmp.workshop.dto.BankCard;
import jmp.workshop.dto.BankCardType;
import jmp.workshop.dto.User;
import jmp.workshop.service.api.Service;
import jmp.workshop.service.api.SubscriptionNotFoundException;

import java.time.LocalDate;
import java.util.ServiceLoader;

import static jmp.workshop.dto.BankCardType.CREDIT;
import static jmp.workshop.service.api.Service.isPayableUser;

public class Application {

    public static void main(String[] args) {
        System.out.println("initialize");
        var bank = loadImplementation(Bank.class);
        var service = loadImplementation(Service.class);
        User user = new User("Bill", "Gates", LocalDate.of(2022, 1, 15));
        var creditCard = bank.createBankCard(new User("Jhon", "Doe", LocalDate.of(1970, 1, 1)), CREDIT);
        System.out.println("make subscription");
        service.subscribe(creditCard);
        System.out.println("-------------");
        System.out.println("verify");
        service.getAllUsers().forEach(System.out::println);
        var creditCardNumber = creditCard.getNumber();
        System.out.println(service.getSubscriptionByBankCardNumber(creditCardNumber)
                .orElseThrow(SubscriptionNotFoundException::new));

        System.out.println(isPayableUser(creditCard.getUser()));
        System.out.println("Average users age " + service.getAverageUsersAge());

        System.out.println(service.getAllSubscriptionsByCondition(subscription -> subscription.getStartDate()
                .isEqual(LocalDate.now())));
    }

    private static <T> T loadImplementation(Class<T> aClass) {
        return ServiceLoader.load(aClass)
                .findFirst()
                .orElseThrow(() -> new RuntimeException("Not loaded " + aClass.getName()));
    }
}
