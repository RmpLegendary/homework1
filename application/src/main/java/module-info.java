module application {
    requires jmp.dto.main;
    requires jmp.cloud.service.impl.main;
    requires jmp.cloud.bank.impl.main;
    requires jmp.bank.api.main;

    uses jmp.workshop.bank.api.Bank;
    uses jmp.workshop.service.api.Service;

    exports jmp.workshop.application;
}