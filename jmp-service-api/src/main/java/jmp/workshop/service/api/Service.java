package jmp.workshop.service.api;

import jmp.workshop.dto.BankCard;
import jmp.workshop.dto.BankCardType;
import jmp.workshop.dto.Subscription;
import jmp.workshop.dto.User;

import java.time.LocalDate;
import java.time.temporal.ChronoUnit;
import java.util.List;
import java.util.Optional;
import java.util.function.Predicate;

public interface Service {
    void subscribe(BankCard bankCard);

    default double getAverageUsersAge() {
        var allUsers = getAllUsers();
        var userCount = allUsers.size();

        if (userCount > 0) {
            return allUsers.stream().mapToLong(Service::getAge).sum() / (double) userCount;
        }
        return 0;
    }

    Optional<Subscription> getSubscriptionByBankCardNumber(String number);

    List<User> getAllUsers();

    private static long getAge(User user) {
        return ChronoUnit.YEARS.between(user.getBirthday(), LocalDate.now());
    }

    List<Subscription> getAllSubscriptionsByCondition(Predicate<Subscription> predicate);

    static boolean isPayableUser(User user) {
        return getAge(user) > 18;
    }
}
